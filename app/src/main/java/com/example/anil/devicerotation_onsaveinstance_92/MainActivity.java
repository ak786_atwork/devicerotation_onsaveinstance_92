package com.example.anil.devicerotation_onsaveinstance_92;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;


// textview  edittext sometimes persist data during rotation  framework does this

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    String data ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.text);

        if (savedInstanceState != null)
        {
            data= savedInstanceState.getString("data");
            editText.setText(data);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("data",editText.getText().toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "ondestroy",Toast.LENGTH_SHORT).show();
    }
}
